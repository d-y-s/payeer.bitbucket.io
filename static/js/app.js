"use strict";

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

/* ^^^
 * Глобальные-вспомогательные функции
 * ========================================================================== */

/**
  * Возвращает HTML-код иконки из SVG-спрайта
  *
  * @param {String} name Название иконки из спрайта
  * @param {Object} opts Объект настроек для SVG-иконки
  *
  * @example SVG-иконка
  * getSVGSpriteIcon('some-icon', {
  *   tag: 'div',
  *   type: 'icons', // colored для подключения иконки из цветного спрайта
  *   class: '', // дополнительные классы для иконки
  *   mode: 'inline', // external для подключаемых спрайтов
  *   url: '', // путь до файла спрайта, необходим только для подключаемых спрайтов
  * });
  */
function getSVGSpriteIcon(name, opts) {
  opts = _extends({
    tag: 'div',
    type: 'icons',
    "class": '',
    mode: 'inline',
    url: ''
  }, opts);
  var external = '';
  var typeClass = '';

  if (opts.mode === 'external') {
    external = "".concat(opts.url, "/sprite.").concat(opts.type, ".svg");
  }

  if (opts.type !== 'icons') {
    typeClass = " svg-icon--".concat(opts.type);
  }

  opts["class"] = opts["class"] ? " ".concat(opts["class"]) : '';
  return "\n    <".concat(opts.tag, " class=\"svg-icon svg-icon--").concat(name).concat(typeClass).concat(opts["class"], "\" aria-hidden=\"true\" focusable=\"false\">\n      <svg class=\"svg-icon__link\">\n        <use xlink:href=\"").concat(external, "#").concat(name, "\"></use>\n      </svg>\n    </").concat(opts.tag, ">\n  ");
}
/* ^^^
 * JQUERY Actions
 * ========================================================================== */


$(function () {
  'use strict';
  /**
   * определение существования элемента на странице
   */

  $.exists = function (selector) {
    return $(selector).length > 0;
  }; //Show selected lang


  function showSelectedLang() {
    var selected = $('.js-selected');
    selected.on('click', function () {
      $('.js-selected-list').fadeToggle();
      $(this).toggleClass('is-active-selected');
    });
  }

  showSelectedLang();
  var PAGE = $('html, body');
  var pageScroller = $('.page-scroller');
  var inMemoryClass = 'page-scroller--memorized';
  var isVisibleClass = 'page-scroller--visible';
  var enabledOffset = 60;
  var pageYOffset = 0;
  var inMemory = false;

  function resetPageScroller() {
    setTimeout(function () {
      if (window.pageYOffset > enabledOffset) {
        pageScroller.addClass(isVisibleClass);
      } else if (!pageScroller.hasClass(inMemoryClass)) {
        pageScroller.removeClass(isVisibleClass);
      }
    }, 150);

    if (!inMemory) {
      pageYOffset = 0;
      pageScroller.removeClass(inMemoryClass);
    }

    inMemory = false;
  }

  if (pageScroller.length > 0) {
    window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
      passive: true
    } : false);
    pageScroller.on('click', function (event) {
      event.preventDefault();
      window.removeEventListener('scroll', resetPageScroller);

      if (window.pageYOffset > 0 && pageYOffset === 0) {
        inMemory = true;
        pageYOffset = window.pageYOffset;
        pageScroller.addClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: 0
        }, 500, 'swing', function () {
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      } else {
        pageScroller.removeClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: pageYOffset
        }, 500, 'swing', function () {
          pageYOffset = 0;
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      }
    });
  }

  $(".check-card__input-file input[type='file']").styler({
    fileBrowse: 'CHOOSE FILE',
    filePlaceholder: 'No file choosen...'
  });
  $('.js-custom-scroll').scrollbox(); //Choose file

  function chooseFile() {
    var btnConfirm = $('.js-btn-confirm');
    $('.jq-file input').change(function () {
      if ($('.jq-file').hasClass('changed')) {
        btnConfirm.addClass('confirm').removeAttr('disabled').text('Confirm Transaction');
      } else {
        btnConfirm.removeClass('confirm').attr('disabled', 'disabled').text('Send Photo');
      }
    });
  }

  chooseFile(); //Delete file

  function deleteFile() {
    $('.js-btn-delete').on('click', function (event) {
      event.preventDefault();
      var fileInputWrp = $(this).closest('.check-card__input-file').find('.jq-file');
      fileInputWrp.after('<input type="file">');
      fileInputWrp.remove();
      $(".check-card__input-file input[type='file']").styler({
        fileBrowse: 'CHOOSE FILE'
      });
      $('.js-btn-confirm').removeClass('confirm').attr('disabled', 'disabled').text('Send Photo');
    });
  }

  deleteFile(); //Contact phone

  function enterContactPhone() {
    var btnConfirm = $('.js-btn-confirm'),
        inputWrpConfirm = $('.js-input-phone-confirm');
    $('#enter_phone').bind("change keyup input click", function () {
      // if (this.value.match(/[^0-9\+]/g)) {
      //     this.value = this.value.replace(/[^0-9\+]/g, '');
      // }
      if ($(this).val().length == 13) {
        inputWrpConfirm.addClass('phone-confirm');
        btnConfirm.addClass('confirm').removeAttr('disabled');
      } else {
        inputWrpConfirm.removeClass('phone-confirm');
        btnConfirm.removeClass('confirm').attr('disabled', 'disabled');
      }
    });
  }

  enterContactPhone(); // Currency Choose

  function btnCurrencyChoose() {
    $('.js-currency-choose').on('click', function () {
      $(this).addClass('is-active-currency').siblings().removeClass('is-active-currency');
    });
  }

  btnCurrencyChoose(); // Copy text

  function copyText() {
    $('.js-btn-copy').on('click', function () {
      var copyText = $('.js-copy-text');
      $(this).siblings(copyText).select();
      document.execCommand('copy');
    });
  }

  copyText();
  $('.js-phone-mask').mask('+000000000000');
});