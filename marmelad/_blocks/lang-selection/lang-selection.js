//Show selected lang

function showSelectedLang() {

    var selected = $('.js-selected');

    selected.on('click', function () {
        $('.js-selected-list').fadeToggle();
        $(this).toggleClass('is-active-selected')
    });
}

showSelectedLang();