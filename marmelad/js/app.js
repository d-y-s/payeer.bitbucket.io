/* ^^^
 * Глобальные-вспомогательные функции
 * ========================================================================== */

/**
  * Возвращает HTML-код иконки из SVG-спрайта
  *
  * @param {String} name Название иконки из спрайта
  * @param {Object} opts Объект настроек для SVG-иконки
  *
  * @example SVG-иконка
  * getSVGSpriteIcon('some-icon', {
  *   tag: 'div',
  *   type: 'icons', // colored для подключения иконки из цветного спрайта
  *   class: '', // дополнительные классы для иконки
  *   mode: 'inline', // external для подключаемых спрайтов
  *   url: '', // путь до файла спрайта, необходим только для подключаемых спрайтов
  * });
  */
function getSVGSpriteIcon(name, opts) {
  opts = Object.assign({
    tag: 'div',
    type: 'icons',
    class: '',
    mode: 'inline',
    url: '',
  }, opts);

  let external = '';
  let typeClass = '';

  if (opts.mode === 'external') {
    external = `${opts.url}/sprite.${opts.type}.svg`;
  }

  if (opts.type !== 'icons') {
    typeClass = ` svg-icon--${opts.type}`;
  }

  opts.class = opts.class ? ` ${opts.class}` : '';

  return `
    <${opts.tag} class="svg-icon svg-icon--${name}${typeClass}${opts.class}" aria-hidden="true" focusable="false">
      <svg class="svg-icon__link">
        <use xlink:href="${external}#${name}"></use>
      </svg>
    </${opts.tag}>
  `;
}

/* ^^^
 * JQUERY Actions
 * ========================================================================== */
$(function() {

  'use strict';

  /**
   * определение существования элемента на странице
   */
  $.exists = (selector) => $(selector).length > 0;

  //=require ../_blocks/**/*.js

    $(".check-card__input-file input[type='file']").styler({
        fileBrowse: 'CHOOSE FILE',
        filePlaceholder: 'No file choosen...'
    });

    $('.js-custom-scroll').scrollbox();

    //Choose file

    function chooseFile() {
        
        var btnConfirm = $('.js-btn-confirm');

        $('.jq-file input').change(function () {
            if ($('.jq-file').hasClass('changed')) {
                btnConfirm.addClass('confirm')
                    .removeAttr('disabled')
                    .text('Confirm Transaction');
            } else {
                btnConfirm.removeClass('confirm')
                    .attr('disabled', 'disabled')
                    .text('Send Photo');
            }
        });

        

    }
    chooseFile();

    //Delete file

    function deleteFile() {
        $('.js-btn-delete').on('click', function (event) {
            event.preventDefault();
            var fileInputWrp = $(this).closest('.check-card__input-file').find('.jq-file');
            fileInputWrp.after('<input type="file">');
            fileInputWrp.remove();
            $(".check-card__input-file input[type='file']").styler({
                fileBrowse: 'CHOOSE FILE'
            });
            $('.js-btn-confirm').removeClass('confirm')
                .attr('disabled', 'disabled')
                .text('Send Photo');
        });
    }
    deleteFile();

    //Contact phone

    function enterContactPhone() {

        var btnConfirm = $('.js-btn-confirm'),
            inputWrpConfirm = $('.js-input-phone-confirm');

        $('#enter_phone').bind("change keyup input click", function () {
            // if (this.value.match(/[^0-9\+]/g)) {
            //     this.value = this.value.replace(/[^0-9\+]/g, '');
            // }

            if ($(this).val().length == 13) {
                inputWrpConfirm.addClass('phone-confirm')
                btnConfirm.addClass('confirm').removeAttr('disabled');
            } else {
                inputWrpConfirm.removeClass('phone-confirm')
                btnConfirm.removeClass('confirm').attr('disabled', 'disabled');
            }
        });

    }
    enterContactPhone()

    // Currency Choose

    function btnCurrencyChoose() {
        $('.js-currency-choose').on('click', function () {
            $(this)
                .addClass('is-active-currency')
                .siblings()
                .removeClass('is-active-currency');
        })
    }
    btnCurrencyChoose();

    // Copy text

    function copyText() {

        $('.js-btn-copy').on('click', function () {
            var copyText = $('.js-copy-text');
            $(this).siblings(copyText).select();
            document.execCommand('copy');
        });
    }
    copyText();

    $('.js-phone-mask').mask('+000000000000');

});
